# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Because I don't like creating Vue.js components manually

# Installation

- [NodeJS via NVM](https://github.com/creationix/nvm)

Install the vuee CLI with this command:

```bash
git clone git@bitbucket.org:Wrongness/vuee.git
cd vuee
npm install -g
```
Sorry no npm package yet.

## Development

```bash
npm install
```

## Production

```bash
cd [vue project]/src/components
vuee component-name [second-component ... ]
```
