#!/usr/bin/env node

const fs = require('fs');

const componentNames = process.argv.length < 3 ? [] : process.argv.slice(2);
const cwd = process.cwd();
const InvalidArgumentError = 9;
const InvalidExecutionPath = 1;

function printUsage() {
  console.error('I need component names.');
  console.error('Type: vuee component-name [second-component ... ]');
}

function isInComponentsDir(dir) {
  return cwd.endsWith('src/components');
}

function isValidateName(name) {
  // TODO: write validation for component names
  if (name !== name.toLowerCase()) return false;
  if (name.indexOf(" ") > -1) return false;
  return true;
}

function componentAlreadyExists(name) {
  return fs.existsSync(`${cwd}/${name}`);
}

function createDirectory(name) {
  fs.mkdirSync(`${cwd}/${name}`);
  console.log(`Directory "${name}" has been created!"`);
}

function writeFile(name, content) {
  fs.writeFile(`${cwd}/${name}`, content, 'utf8', (err) => {
    if (err) throw err;
    console.log(`File "${name}" has been saved!`);
  });
}

function createVueFile(name) {
  const template = `<template>
  <div class="${name}"></div>
</template>
<script src="./${name}.js"></script>
<style src="./${name}.scss" lang="scss"></style>
`;
  writeFile(`${name}/${name}.vue`, template);
}

function createJsFile(name) {
  const template = `export default {
  // props: {
  //   name: String,
  // },
  // components: { childComponent },
  data() {
    return {};
  },
};
`;
  writeFile(`${name}/${name}.js`, template);
}

function createScssFile(name) {
  const template = `.${name} {
  color: red;
  background-color: lime;
}
`;
  writeFile(`${name}/${name}.scss`, template);
}


function createFiles(name) {
  // TODO: test for errors while file creation, write rights
  createDirectory(name);
  createScssFile(name);
  createJsFile(name);
  createVueFile(name);
}

function makeComponent(name) {
  if (isValidateName(name)) {
    if (componentAlreadyExists(name)) {
      console.warn(`Component "${name}" already exists, skipping ...`);
    } else {
      console.log(`Creating component: ${name}`);
      createFiles(name);
    }
  } else {
    console.warn(`Component name "${name}" is not valid, skipping ...`);
  }
}

if (componentNames.length) {
  // has to be at least one component name
  isInComponentsDir();
  if (isInComponentsDir(cwd)) {
    componentNames.forEach(makeComponent);
  } else {
    console.error('Can only be executed in components folder !!!');
    console.error(`You are in "${cwd}"`);
    process.exit(InvalidExecutionPath);
  }
} else {
  printUsage();
  process.exit(InvalidArgumentError);
}
